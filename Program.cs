﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Net;
using System.Text;

namespace Module2
{
    public class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.UTF8;
            Console.WriteLine("Hello World!");

            Program program = new Program();
            
            //* Exercise 1

            Console.Write("\nEnter the companies Number:\t");
            int companiesNumber = Convert.ToInt32(Console.ReadLine());

            Console.Write("\nEnter the tax number:\t");
            int tax = Convert.ToInt32(Console.ReadLine());

            Console.Write("\nEnter the company Revenue:\t");
            int companyRevenue = Convert.ToInt32(Console.ReadLine());

            Console.Write("\n Income: ");
            Console.WriteLine(program.GetTotalTax(companiesNumber, tax, companyRevenue));

            //* Exercise 2

            Console.Write("\nEnter your age:\t");
            int input = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine(program.GetCongratulation(input));

            //* Exercise 3

            Console.Write("\nEnter first number:\t");
            string first = Console.ReadLine().Replace(",", ".");

            Console.Write("\nEnter second number:\t");
            string second = Console.ReadLine().Replace(",", ".");

            Console.WriteLine(program.GetMultipliedNumbers(first, second));

            //* Exercise 4

            Console.Write("\nEnter figure type:\t");
            string figure_type = Console.ReadLine();
            Enum.TryParse(figure_type, out Figure figureType);

            Console.Write("\nEnter parameter to compute:\t");
            string parameter = Console.ReadLine();
            Enum.TryParse(parameter, out Parameter parameterToCompute);

            Dimensions dimensions = new Dimensions();

            Console.Write("\nEnter First Side:\t");
            dimensions.FirstSide = Convert.ToDouble(Console.ReadLine());

            Console.Write("\nEnter Second Side:\t");
            dimensions.SecondSide = Convert.ToDouble(Console.ReadLine());

            Console.Write("\nEnter Third Side:\t");
            dimensions.ThirdSide = Convert.ToDouble(Console.ReadLine());

            Console.Write("\nEnter Height:\t");
            dimensions.Height = Convert.ToDouble(Console.ReadLine());

            Console.Write("\nEnter Diameter:\t");
            dimensions.Diameter = Convert.ToDouble(Console.ReadLine());
                
            Console.Write("\nEnter Radius:\t");
            dimensions.Radius = Convert.ToDouble(Console.ReadLine());

            Console.Write("\n Result: ");
            Console.WriteLine(program.GetFigureValues(figureType, parameterToCompute, dimensions));
        }

        public int GetTotalTax(int companiesNumber, int tax, int companyRevenue)
        {
            return  companiesNumber * tax * companyRevenue / 100;
        }

        public string GetCongratulation(int input)
        {
            string congratulation;

            if (input % 2 == 0 && input >= 18)
            {
                congratulation = "Поздравляю с совершеннолетием!";
            }
            else if (input % 2 != 0 && input < 18 && input > 12)
            {
                congratulation = "Поздравляю с переходом в старшую школу!";
            }
            else
            {
                congratulation = $"Поздравляю c {input}-летием!";
            }

            return congratulation;
        }

        public double GetMultipliedNumbers(string first, string second)
        {
            double result = 0;
            if (double.TryParse(first, out double first_num) && double.TryParse(second, out double second_num))
                result = first_num * second_num;
            return result;
        }

        public double GetFigureValues(Figure figureType, Parameter parameterToCompute, Dimensions dimensions)
        {
            double value = 0;
            switch (figureType)
            {
                case Figure.Triangle:
                    if (parameterToCompute == Parameter.Perimeter)
                    {
                        value = dimensions.FirstSide + dimensions.SecondSide + dimensions.ThirdSide;
                    }
                    else
                    {
                        if (dimensions.Height == 0)
                        {
                            double semi_perimeter = (dimensions.FirstSide + dimensions.SecondSide + dimensions.ThirdSide) / 2;
                            value = Math.Sqrt(semi_perimeter * (semi_perimeter - dimensions.FirstSide) * (semi_perimeter - dimensions.SecondSide) * (semi_perimeter - dimensions.ThirdSide));
                        }
                        else
                        {
                            value = dimensions.FirstSide * dimensions.Height / 2;
                        }
                    }

                    break;
                case Figure.Rectangle:
                    if (parameterToCompute == Parameter.Square)
                    {
                        value = dimensions.FirstSide * dimensions.SecondSide;
                    }
                    else
                    {
                        value = (dimensions.FirstSide + dimensions.SecondSide) * 2;
                    }

                    break;
                case Figure.Circle:
                    if (parameterToCompute == Parameter.Perimeter)
                    {
                        value = 2 * 3.14 * dimensions.Radius;
                    }
                    else
                    {
                        value = dimensions.Radius * dimensions.Radius * 3.14;
                    }

                    break;
            }

            return Math.Round(value, MidpointRounding.AwayFromZero);
        }
    }
}
